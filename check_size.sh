#!/bin/bash

DIR=""
EMAIL=""
OUTPUT=""
declare -A FILES_NAMES
declare -A FILES_SIZES
declare -A FILES_OLD_SIZES

usage() {                                
  echo "Usage: $0 [ --dir DIRPATH ] [ --email EMAIL@EXAMPLE.COM ]" #1>&2 
}

help() {
	usage
	echo -e "
	--help\t- this view\n
	--dir\t- set directory to check, if not provided check current directory\n
	--email\t- send report to this user's, if not provided output will be displayed on the output only\n"
}

get_historical_size(){
	while read line
	do 
		if [[ $line == *"$1"* ]]
		then
			echo "${line#*;}"
			return
		fi
	done < .check_size.db

	echo "nan"
	return
}
save_database(){
	RET=""
	i=0
	for file in "${FILES_NAMES[@]}"
	do
		RET="$RET${FILES_NAMES[$i]};${FILES_SIZES[$i]}\n"
		i=$((i+1))
	done
	echo -en ${RET} > .check_size.db
}

create_output()
{
	i=0
	OUTPUT="SIZE\tNAME\tCHANGE\r\n"
	for file in "${FILES_NAMES[@]}"
	do
		CHANGE_VALUE="NaN"
		if [ ${FILES_OLD_SIZES[$i]} -ne 0 ]
		then
			CHANGE_VALUE="$(((FILES_SIZES[$i]-FILES_OLD_SIZES[$i])*100/FILES_OLD_SIZES[$i]))%%"
		fi
		OUTPUT="$OUTPUT${FILES_SIZES[$i]}\t${FILES_NAMES[$i]}\t$CHANGE_VALUE\r\n"
		i=$((i+1))
	done
}
send_email(){
	if [ -z "$OUTPUT" ]
	then
		create_output
	fi
	echo -e ${OUTPUT} | mail -s "[$(hostname)] - size of ${DIR}" $EMAIL
}

print_output(){
	if [ -z "$OUTPUT" ]
	then
		create_output
	fi
	printf $OUTPUT
}

check_mail_service(){
	if ! command -v mail &> /dev/null
	then
		echo "mail could not be found"
		echo "you can install it with:"
		echo "sudo apt install mailutils"
		exit
	fi

	if [[ $(groups) != *"mail"* ]]
	then
		echo "user cannot send mails"
		echo "please add user to mail group with:"
		echo "sudo adduser <USERNAME> mail"
		exit
	fi
}

check_mail_service

ARGS=$(getopt -o :d:e:h --long dir:,email:,help -- "$@")
eval set -- "$ARGS"

while true; do
  case "$1" in
    -d | --dir ) 
		# MEMORY="$2"; 
		DIR="$2"
		shift 2 ;;
	-e | --email)
		EMAIL="$EMAIL$2, "
		shift 2 ;;
	-h | --help)
		help
		exit 0
		shift ;;
    (--) 
		shift
		break ;;
    (*) 
		usage
		exit 1
		break ;;
  esac
done

if [ -z "$DIR" ]
then
	DIR=$(pwd)
fi

DIR=$(realpath $DIR)

cd $DIR

i=0
for f in *; do
  FILES_NAMES[$i]="$f"
  FILES_SIZES[$i]="$(stat --printf='%s' $f)"
  bla="$(get_historical_size $f)"
  FILES_OLD_SIZES[$i]=$bla
  i=$((i+1))
done

save_database
print_output

if [ -n "$EMAIL" ]
then
	send_email
fi

exit 0